import React, { Component } from 'react';
import './home.css';
import Card from '../card';

const allImages = require('../../data/images.json');
const indexLimit = Math.ceil(allImages.length / 12);
console.log("data", allImages, indexLimit);
class Home extends Component {
    constructor() {
        super();
        let splicedImages = allImages.filter(function (f) { return f; })
        this.state = {
            range: 1000,
            genderType: 'all',
            categoryType: 'all',
            index: 0,
            imagesList: splicedImages.splice(0, 12),
            finalArray: allImages,
            indexLimit: indexLimit,
            hoveredImgIndex: null
        }
    }

    updateTextInput = (e) => {
        let allImgs = [];
        if (this.state.categoryType !== 'all' && this.state.genderType !== 'all') {
            allImages.map((img, i) => {
                if (img.category === this.state.categoryType && img.type === this.state.genderType) {
                    allImgs.push(img);
                }
            });
        } else if (this.state.categoryType !== 'all') {
            allImages.map((img, i) => {
                if (img.category === this.state.categoryType) {
                    allImgs.push(img);
                }
            });
        } else if (this.state.genderType !== 'all') {
            allImages.map((img, i) => {
                if (img.type === this.state.genderType) {
                    allImgs.push(img);
                }
            });
        }
        else {
            allImgs = allImages;
        }
        this.setState({
            range: e.target.value,
            index: 0,
            indexLimit: indexLimit
        }, function () {
            if (this.state.range > 0) {
                let filteredImages = [];
                allImgs.map((img, i) => {
                    if (img.price <= this.state.range) {
                        filteredImages.push(img);
                    }
                });
                let splicedImages = filteredImages.filter(function (f) { return f; })
                this.setState({
                    imagesList: splicedImages.splice((this.state.index * 12), 12),
                    finalArray: filteredImages,
                    indexLimit: Math.ceil(filteredImages.length / 12)
                });
            } else {
                this.setState({
                    imagesList: [],
                    finalArray: [],
                    indexLimit: 0
                });
            }
        });
    }
    genderSelect(e) {
        let allImgs = [];
        if (this.state.categoryType !== 'all' && this.state.range > 0) {
            allImages.map((img, i) => {
                if (img.category === this.state.categoryType && img.price <= this.state.range) {
                    allImgs.push(img);
                }
            });
        } else if (this.state.categoryType !== 'all') {
            allImages.map((img, i) => {
                if (img.category === this.state.categoryType) {
                    allImgs.push(img);
                }
            });
        } else if (this.state.range > 0) {
            allImages.map((img, i) => {
                if (img.price <= this.state.range) {
                    allImgs.push(img);
                }
            });
        }
        else {
            allImgs = allImages;
        }

        this.setState({
            genderType: e,
            index: 0,
            indexLimit: indexLimit
        }, function () {
            if (e === 'all') {
                let splicedImages = allImgs.filter(function (f) { return f; })
                this.setState({
                    imagesList: splicedImages.splice((this.state.index * 12), 12),
                    finalArray: allImgs,
                    indexLimit: Math.ceil(allImages.length / 12)
                });
            } else {
                let filteredImages = [];
                allImgs.map((img, i) => {
                    if (img.type === e) {
                        filteredImages.push(img);
                    }
                });
                let splicedImages = filteredImages.filter(function (f) { return f; })
                this.setState({
                    imagesList: splicedImages.splice((this.state.index * 12), 12),
                    finalArray: filteredImages,
                    indexLimit: Math.ceil(filteredImages.length / 12)
                });
            }
        });
    }
    onCategorySelect = (e) => {
        let filteredImages = [], allImgs = [];
        if (this.state.genderType !== 'all' && this.state.range > 0) {
            allImages.map((img, i) => {
                if (img.type === this.state.genderType && img.price <= this.state.range) {
                    allImgs.push(img);
                }
            });
        } else if (this.state.genderType !== 'all') {
            allImages.map((img, i) => {
                if (img.type === this.state.genderType) {
                    allImgs.push(img);
                }
            });
        } else if (this.state.range > 0) {
            allImages.map((img, i) => {
                if (img.price <= this.state.range) {
                    allImgs.push(img);
                }
            });
        } else {
            allImgs = allImages;
        }
        this.setState({
            categoryType: e.target.value,
            index: 0,
            indexLimit: indexLimit
        }, function () {
            if (this.state.categoryType === "all") {
                let splicedImages = allImgs.filter(function (f) { return f; })
                this.setState({
                    imagesList: splicedImages.splice((this.state.index * 12), 12),
                    finalArray: allImgs,
                    indexLimit: Math.ceil(allImages.length / 12)
                });
            } else {
                allImgs.map((img, i) => {
                    if (this.state.categoryType === img.category) {
                        filteredImages.push(img);
                    }
                });
                let splicedImages = filteredImages.filter(function (f) { return f; })
                this.setState({
                    imagesList: splicedImages.splice((this.state.index * 12), 12),
                    finalArray: filteredImages,
                    indexLimit: Math.ceil(filteredImages.length / 12)
                });
            }
        });
    }
    goPrev = (e) => {
        if (this.state.index !== 0) {
            let splicedImages;
            if (this.state.genderType !== 'all' || this.state.categoryType !== 'all' || this.state.range > 0) {
                splicedImages = this.state.finalArray.filter(function (f) { return f; });
            } else {
                splicedImages = allImages.filter(function (f) { return f; })
            }
            this.setState({
                index: (this.state.index <= this.state.indexLimit) ? this.state.index - 1 : 0
            }, function () {
                this.setState({
                    imagesList: splicedImages.splice((this.state.index * 12), 12)
                });
            });
        }
    }
    goNext = () => {
        if (this.state.index !== (this.state.indexLimit - 1)) {
            let splicedImages;
            if (this.state.genderType !== 'all' || this.state.categoryType !== 'all' || this.state.range > 0) {
                splicedImages = this.state.finalArray.filter(function (f) { return f; });
            } else {
                splicedImages = allImages.filter(function (f) { return f; })
            }
            this.setState({
                index: (this.state.index < this.state.indexLimit) ? this.state.index + 1 : 0
            }, function () {
                this.setState({
                    imagesList: splicedImages.splice((this.state.index * 12), 12)
                });
            });
        }
    }
    goToAmazon = () => {
        // window.open("https://www.amazon.in/", "_blank");
    }
    imageHover = (index) => {
        this.setState({
            hoveredImgIndex: index
        });
    }
    imageOut = () => {
        this.setState({
            hoveredImgIndex: null
        });
    }

    render() {
        const { range, genderType, categoryType, imagesList, index, indexLimit, hoveredImgIndex } = this.state;
        return (
            <div className="main-container">
                <div className="title-block">
                    <div className="first-title">
                        <h1>Christmas Gifts</h1>
                    </div>
                    <div className="category-block">
                        <label className="label-text">Select category</label>
                        <select value={categoryType} onChange={this.onCategorySelect}>
                            <option value="all">All</option>
                            <option value="Dresses">Dresses</option>
                            <option value="Shoes">Shoes</option>
                            <option value="Bags">Bags</option>
                        </select>
                    </div>
                    <div className="gift-for">
                        <div className="label-text">gift for</div>
                        <br />
                        <div className="gender-block" onClick={() => this.genderSelect("all")} >
                            <img src='images/all.png' alt="all" className={genderType === "all" ? "image-active" : ""}></img>
                            <p className={genderType === "all" ? "text-red" : ""}> All</p>
                        </div>
                        <div className="gender-block" onClick={() => this.genderSelect("men")}>
                            <img src='images/men.png' alt="men"></img>
                            <p className={genderType === "men" ? "text-red" : ""}> Men</p></div>
                        <div className="gender-block" onClick={() => this.genderSelect("women")}>
                            <img src='images/women.png' alt="women"></img>
                            <p className={genderType === "women" ? "text-red" : ""}> Women</p>
                        </div>
                        <div className="gender-block" onClick={() => this.genderSelect("kids")}>
                            <img src='images/kids.png' alt="kid"></img>
                            <p className={genderType === "kids" ? "text-red" : ""}> Kids</p>
                        </div>
                        <div className="gender-block" onClick={() => this.genderSelect("teen")}>
                            <img src='images/teens.png' alt="teen"></img>
                            <p className={genderType === "teen" ? "text-red" : ""}> Teen</p>
                        </div>
                    </div>
                    <div className="set-price">
                        <label className="label-text" htmlFor="range">set price</label>
                        <input type="range" id="range" name="points" min="0" max="1000" value={range} onChange={this.updateTextInput}></input>
                        <div className="price-list">
                            <span>$0</span>
                            <span>$250</span>
                            <span>$500</span>
                            <span>$750</span>
                            <span>$1000</span>
                        </div>
                    </div>
                </div>
                <div className="main-img-container">
                    <div onClick={this.goPrev} className={index === 0 ? 'isDisabled arrow-div1' : 'arrow-div1'} style={{ display: indexLimit > 1 ? 'block' : 'none' }}>
                       <i className="fa fa-angle-left fa-2x"></i>
                    </div>
                    {imagesList.length > 0 ?
                        <div className="img-container">
                            {imagesList.map((img, i) =>
                                <div className="img-item" key={i} onClick={this.goToAmazon} onMouseOver={() => this.imageHover(i)} onMouseLeave={() => this.imageOut()}>
                                    <img src={'images/' + img.type + '/' + img.name + '.jpg'} alt="teen"></img>
                                    <div className="text-left c-title">Amazon</div>
                                    <div className="text-left c-price">${img.price}</div>
                                    {hoveredImgIndex === i ?
                                        <Card image={img} />
                                        : null}
                                </div>
                            )}
                        </div>
                        :
                        <div className="img-container-empty">
                            No results found
                        </div>
                    }
                    <div onClick={this.goNext} className={index === (indexLimit - 1) ? 'isDisabled arrow-div2' : 'arrow-div2'} style={{ display: indexLimit > 1 ? 'block' : 'none' }}>
                        <i className="fa fa-angle-right  fa-2x"></i>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home;