import React from 'react';

const card = (props) => {
    return (
        <div className="img-card">
            <img src={'images/' + props.image.type + '/' + props.image.name + '.jpg'} alt="teen" width="100%" height="100%"></img>
            <div className="text-left cust-name">{props.image.name}</div>
            <del className="text-left cust-price1">$ {props.image.price}</del>
            <div className="text-left cust-price2">$ {props.image.price}</div>
            <div className="text-left cust-name2">Macy's</div>
            <p className="titlebox">See more <span>{props.image.type}</span> Apparel <img className="title-arrow" src='images/white-arrow-right.png' alt="right"></img></p>
        </div>
    )
}

export default card